<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use PhpExtended\Version\VersionInterface;

/**
 * OperatingSystem class file.
 * 
 * This is a simple implementation of the OperatingSystemInterface.
 * 
 * @author Anastaszor
 */
class OperatingSystem implements OperatingSystemInterface
{
	
	/**
	 * The name of the operating system.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The version of the operating system.
	 * 
	 * @var VersionInterface
	 */
	protected VersionInterface $_version;
	
	/**
	 * The family of the operating system.
	 * 
	 * @var OperatingSystemFamilyInterface
	 */
	protected OperatingSystemFamilyInterface $_family;
	
	/**
	 * Builds a new OperatingSystem with the given version and family.
	 * 
	 * @param string $name
	 * @param VersionInterface $version
	 * @param OperatingSystemFamilyInterface $family
	 */
	public function __construct(string $name, VersionInterface $version, OperatingSystemFamilyInterface $family)
	{
		$this->_name = $name;
		$this->_version = $version;
		$this->_family = $family;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name.'/'.$this->_version->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemInterface::getVersion()
	 */
	public function getVersion() : VersionInterface
	{
		return $this->_version;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemInterface::getFamily()
	 */
	public function getFamily() : OperatingSystemFamilyInterface
	{
		return $this->_family;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemInterface::equals()
	 */
	public function equals($operatingSystem) : bool
	{
		return $operatingSystem instanceof OperatingSystemInterface
			&& $this->getName() === $operatingSystem->getName()
			&& $this->getVersion()->equals($operatingSystem->getVersion())
			&& $this->getFamily()->equals($operatingSystem->getFamily());
	}
	
}
