<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

/**
 * DeviceType class file.
 * 
 * This is a simple implementation of the DeviceTypeInterface.
 * 
 * @author Anastaszor
 */
class DeviceType implements DeviceTypeInterface
{
	
	/**
	 * The name of the device type.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The name of the device type.
	 * 
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\DeviceTypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\DeviceTypeInterface::equals()
	 */
	public function equals($type) : bool
	{
		return $type instanceof DeviceTypeInterface
			&& $this->getName() === $type->getName();
	}
	
}
