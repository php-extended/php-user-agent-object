<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

/**
 * Device class file.
 * 
 * This is a simple implementation of the DeviceInterface.
 * 
 * @author Anastaszor
 */
class Device implements DeviceInterface
{
	
	/**
	 * The name of the device.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The device type of this device.
	 * 
	 * @var DeviceTypeInterface
	 */
	protected DeviceTypeInterface $_deviceType;
	
	/**
	 * The brand of this device.
	 * 
	 * @var BrandInterface
	 */
	protected BrandInterface $_brand;
	
	/**
	 * Builds a new Device with its data.
	 * 
	 * @param string $name
	 * @param DeviceTypeInterface $deviceType
	 * @param BrandInterface $brand
	 */
	public function __construct(string $name, DeviceTypeInterface $deviceType, BrandInterface $brand)
	{
		$this->_name = $name;
		$this->_deviceType = $deviceType;
		$this->_brand = $brand;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\DeviceInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\DeviceInterface::getDeviceType()
	 */
	public function getDeviceType() : DeviceTypeInterface
	{
		return $this->_deviceType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\DeviceInterface::getBrand()
	 */
	public function getBrand() : BrandInterface
	{
		return $this->_brand;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\DeviceInterface::equals()
	 */
	public function equals($device) : bool
	{
		return $device instanceof DeviceInterface
			&& $this->getName() === $device->getName()
			&& $this->getDeviceType()->equals($device->getDeviceType())
			&& $this->getBrand()->equals($device->getBrand());
	}
	
}
