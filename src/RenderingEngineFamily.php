<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

/**
 * RenderingEngineFamily class file.
 * 
 * This is a simple implementation of the RenderingEngineFamilyInterface.
 * 
 * @author Anastaszor
 */
class RenderingEngineFamily implements RenderingEngineFamilyInterface
{
	
	/**
	 * The name of the rendering engine family.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new RenderingEngineFamily with the given data.
	 * 
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\RenderingEngineFamilyInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\RenderingEngineFamilyInterface::equals()
	 */
	public function equals($family) : bool
	{
		return $family instanceof RenderingEngineFamilyInterface
			&& $this->getName() === $family->getName();
	}
	
}
