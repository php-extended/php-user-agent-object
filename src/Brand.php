<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

/**
 * Brand class file.
 * 
 * This is a simple implementation of the brand interface.
 * 
 * @author Anastaszor
 */
class Brand implements BrandInterface
{
	
	/**
	 * The name of the brand.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new Brand with the given name.
	 * 
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\BrandInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\BrandInterface::equals()
	 */
	public function equals($brand) : bool
	{
		return $brand instanceof BrandInterface
			&& $this->getName() === $brand->getName();
	}
	
}
