<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

/**
 * UserAgentType class file.
 * 
 * This is a simple implementation of the UserAgentTypeInterface.
 * 
 * @author Anastaszor
 */
class UserAgentType implements UserAgentTypeInterface
{
	
	/**
	 * The name of the user agent type.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new UserAgentType with the given name.
	 * 
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->_name = $name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentTypeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentTypeInterface::equals()
	 */
	public function equals($type) : bool
	{
		return $type instanceof UserAgentTypeInterface
			&& $this->getName() === $type->getName();
	}
	
}
