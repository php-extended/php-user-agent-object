<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use DateTimeInterface;
use PhpExtended\Version\VersionInterface;

/**
 * UserAgent class file.
 * 
 * This is a simple implementation of the UserAgentInterface.
 * 
 * @author Anastaszor
 */
class UserAgent implements UserAgentInterface
{
	
	/**
	 * The name of the user agent.
	 * 
	 * @var ?string
	 */
	protected ?string $_name = null;
	
	/**
	 * The type of the user agent.
	 * 
	 * @var ?UserAgentTypeInterface
	 */
	protected ?UserAgentTypeInterface $_type = null;
	
	/**
	 * The version of the user agent.
	 * 
	 * @var ?VersionInterface
	 */
	protected ?VersionInterface $_version = null;
	
	/**
	 * The device of the user agent.
	 * 
	 * @var ?DeviceInterface
	 */
	protected ?DeviceInterface $_device = null;
	
	/**
	 * The operating system of the user agent.
	 * 
	 * @var ?OperatingSystemInterface
	 */
	protected ?OperatingSystemInterface $_operatingSystem = null;
	
	/**
	 * The rendering engine of the user agent.
	 * 
	 * @var ?RenderingEngineInterface
	 */
	protected ?RenderingEngineInterface $_renderingEngine = null;
	
	/**
	 * The apparition date of the user agent.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_apparitionDate = null;
	
	/**
	 * The disparition date of the user agent.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_disparitionDate = null;
	
	/**
	 * The header value of the user agent.
	 * 
	 * @var string
	 */
	protected string $_headerValue;
	
	/**
	 * Builds a new UserAgent with the given header value.
	 * 
	 * @param string $headerValue
	 */
	public function __construct(string $headerValue)
	{
		$this->_headerValue = $headerValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_headerValue;
	}
	
	/**
	 * Sets the name of this user agent.
	 * 
	 * @param ?string $name
	 * @return UserAgentInterface
	 */
	public function setName(?string $name) : UserAgentInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getName()
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the type of this user agent.
	 * 
	 * @param ?UserAgentTypeInterface $type
	 * @return UserAgentInterface
	 */
	public function setType(?UserAgentTypeInterface $type) : UserAgentInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getType()
	 */
	public function getType() : ?UserAgentTypeInterface
	{
		return $this->_type;
	}
	
	/**
	 * Sets the version of this user agent.
	 * 
	 * @param ?VersionInterface $version
	 * @return UserAgentInterface
	 */
	public function setVersion(?VersionInterface $version) : UserAgentInterface
	{
		$this->_version = $version;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getVersion()
	 */
	public function getVersion() : ?VersionInterface
	{
		return $this->_version;
	}
	
	/**
	 * Sets the device of this user agent.
	 * 
	 * @param ?DeviceInterface $device
	 * @return UserAgentInterface
	 */
	public function setDevice(?DeviceInterface $device) : UserAgentInterface
	{
		$this->_device = $device;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getDevice()
	 */
	public function getDevice() : ?DeviceInterface
	{
		return $this->_device;
	}
	
	/**
	 * Sets the operating system of this user agent.
	 * 
	 * @param ?OperatingSystemInterface $operatingSystem
	 * @return UserAgentInterface
	 */
	public function setOperatingSystem(?OperatingSystemInterface $operatingSystem) : UserAgentInterface
	{
		$this->_operatingSystem = $operatingSystem;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getOperatingSystem()
	 */
	public function getOperatingSystem() : ?OperatingSystemInterface
	{
		return $this->_operatingSystem;
	}
	
	/**
	 * Sets the rendering engine of this user agent.
	 * 
	 * @param ?RenderingEngineInterface $renderingEngine
	 * @return UserAgentInterface
	 */
	public function setRenderingEngine(?RenderingEngineInterface $renderingEngine) : UserAgentInterface
	{
		$this->_renderingEngine = $renderingEngine;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getRenderingEngine()
	 */
	public function getRenderingEngine() : ?RenderingEngineInterface
	{
		return $this->_renderingEngine;
	}
	
	/**
	 * Sets the apparition date of this user agent.
	 * 
	 * @param ?DateTimeInterface $date
	 * @return UserAgentInterface
	 */
	public function setApparitionDate(?DateTimeInterface $date) : UserAgentInterface
	{
		$this->_apparitionDate = $date;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getApparitionDate()
	 */
	public function getApparitionDate() : ?DateTimeInterface
	{
		return $this->_apparitionDate;
	}
	
	/**
	 * Sets the disparition date of this user agent.
	 * 
	 * @param DateTimeInterface $date
	 * @return UserAgentInterface
	 */
	public function setDisparitionDate(?DateTimeInterface $date) : UserAgentInterface
	{
		$this->_disparitionDate = $date;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getDisparitionDate()
	 */
	public function getDisparitionDate() : ?DateTimeInterface
	{
		return $this->_disparitionDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::getHeaderValue()
	 */
	public function getHeaderValue() : string
	{
		return $this->_headerValue;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\UserAgentInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof UserAgentInterface
			&& $this->getHeaderValue() === $other->getHeaderValue();
	}
	
}
