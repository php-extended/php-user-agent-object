<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use PhpExtended\Version\VersionInterface;

/**
 * RenderingEngine class file.
 * 
 * This is a simple implementation of the RenderingEngineInterface.
 * 
 * @author Anastaszor
 */
class RenderingEngine implements RenderingEngineInterface
{
	
	/**
	 * The name of the rendering engine.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The version of the rendering engine.
	 * 
	 * @var VersionInterface
	 */
	protected VersionInterface $_version;
	
	/**
	 * The family of the rendering engine.
	 * 
	 * @var RenderingEngineFamilyInterface
	 */
	protected RenderingEngineFamilyInterface $_family;
	
	/**
	 * Builds a new RenderingEngine with the given data.
	 * 
	 * @param string $name
	 * @param VersionInterface $version
	 * @param RenderingEngineFamilyInterface $family
	 */
	public function __construct(string $name, VersionInterface $version, RenderingEngineFamilyInterface $family)
	{
		$this->_name = $name;
		$this->_version = $version;
		$this->_family = $family;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name.'/'.$this->_version->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\RenderingEngineInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\RenderingEngineInterface::getVersion()
	 */
	public function getVersion() : VersionInterface
	{
		return $this->_version;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\RenderingEngineInterface::getFamily()
	 */
	public function getFamily() : RenderingEngineFamilyInterface
	{
		return $this->_family;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\RenderingEngineInterface::equals()
	 */
	public function equals($renderingEngine) : bool
	{
		return $renderingEngine instanceof RenderingEngineInterface
			&& $this->getName() === $renderingEngine->getName()
			&& $this->getVersion()->equals($renderingEngine->getVersion())
			&& $this->getFamily()->equals($renderingEngine->getFamily());
	}
	
}
