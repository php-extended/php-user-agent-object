<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

/**
 * OperatingSystemFamily class file.
 * 
 * This is a simple implementation of the OperatingSystemFamilyInterface.
 * 
 * @author Anastaszor
 */
class OperatingSystemFamily implements OperatingSystemFamilyInterface
{
	
	/**
	 * The name of the operating system family.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The brand of the operating system family.
	 * 
	 * @var BrandInterface
	 */
	protected BrandInterface $_brand;
	
	/**
	 * Builds a new OperatingSystemFamily with the given brand.
	 * 
	 * @param string $name
	 * @param BrandInterface $brand
	 */
	public function __construct(string $name, BrandInterface $brand)
	{
		$this->_name = $name;
		$this->_brand = $brand;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemFamilyInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemFamilyInterface::getBrand()
	 */
	public function getBrand() : BrandInterface
	{
		return $this->_brand;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\UserAgent\OperatingSystemFamilyInterface::equals()
	 */
	public function equals($family) : bool
	{
		return $family instanceof OperatingSystemFamilyInterface
			&& $this->getName() === $family->getName()
			&& $this->getBrand()->equals($family->getBrand());
	}
	
}
