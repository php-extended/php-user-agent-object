<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\Brand;
use PHPUnit\Framework\TestCase;

/**
 * BrandTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\Brand
 *
 * @internal
 *
 * @small
 */
class BrandTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Brand
	 */
	protected Brand $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('BRAND', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('BRAND', $this->_object->getName());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals(new Brand('BRAND')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Brand('BRAND');
	}
	
}
