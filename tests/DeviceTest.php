<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\Brand;
use PhpExtended\UserAgent\Device;
use PhpExtended\UserAgent\DeviceType;
use PHPUnit\Framework\TestCase;

/**
 * DeviceTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\Device
 *
 * @internal
 *
 * @small
 */
class DeviceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Device
	 */
	protected Device $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('PHONE', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('PHONE', $this->_object->getName());
	}
	
	public function testGetDeviceType() : void
	{
		$this->assertEquals(new DeviceType('MOBILE'), $this->_object->getDeviceType());
	}
	
	public function testGetBrand() : void
	{
		$this->assertEquals(new Brand('BRAND'), $this->_object->getBrand());
	}
	
	public function testEquals() : void
	{
		$device = new Device('PHONE', new DeviceType('MOBILE'), new Brand('BRAND'));
		
		$this->assertTrue($this->_object->equals($device));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Device('PHONE', new DeviceType('MOBILE'), new Brand('BRAND'));
	}
	
}
