<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\UserAgentType;
use PHPUnit\Framework\TestCase;

/**
 * UserAgentTypeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\UserAgentType
 *
 * @internal
 *
 * @small
 */
class UserAgentTypeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UserAgentType
	 */
	protected UserAgentType $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('BOT', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('BOT', $this->_object->getName());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals(new UserAgentType('BOT')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UserAgentType('BOT');
	}
	
}
