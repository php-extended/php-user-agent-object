<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\Brand;
use PhpExtended\UserAgent\OperatingSystem;
use PhpExtended\UserAgent\OperatingSystemFamily;
use PhpExtended\Version\Version;
use PHPUnit\Framework\TestCase;

/**
 * OperatingSystemTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\OperatingSystem
 *
 * @internal
 *
 * @small
 */
class OperatingSystemTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OperatingSystem
	 */
	protected OperatingSystem $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('DEBIAN/1.0.1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('DEBIAN', $this->_object->getName());
	}
	
	public function testGetVersion() : void
	{
		$this->assertEquals(new Version(1, 0, 1), $this->_object->getVersion());
	}
	
	public function testGetFamily() : void
	{
		$expected = new OperatingSystemFamily('LINUX', new Brand('BRAND'));
		
		$this->assertEquals($expected, $this->_object->getFamily());
	}
	
	public function testEquals() : void
	{
		$operatingSystem = new OperatingSystem('DEBIAN', new Version(1, 0, 1), new OperatingSystemFamily('LINUX', new Brand('BRAND')));
		
		$this->assertTrue($this->_object->equals($operatingSystem));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OperatingSystem('DEBIAN', new Version(1, 0, 1), new OperatingSystemFamily('LINUX', new Brand('BRAND')));
	}
	
}
