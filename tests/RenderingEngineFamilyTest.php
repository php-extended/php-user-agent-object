<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\RenderingEngineFamily;
use PHPUnit\Framework\TestCase;

/**
 * RenderingEngineFamilyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\RenderingEngineFamily
 *
 * @internal
 *
 * @small
 */
class RenderingEngineFamilyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RenderingEngineFamily
	 */
	protected RenderingEngineFamily $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('KHTML', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('KHTML', $this->_object->getName());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals(new RenderingEngineFamily('KHTML')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RenderingEngineFamily('KHTML');
	}
	
}
