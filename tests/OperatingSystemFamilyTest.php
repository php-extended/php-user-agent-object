<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\Brand;
use PhpExtended\UserAgent\OperatingSystemFamily;
use PHPUnit\Framework\TestCase;

/**
 * OperatingSystemFamilyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\OperatingSystemFamily
 *
 * @internal
 *
 * @small
 */
class OperatingSystemFamilyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OperatingSystemFamily
	 */
	protected OperatingSystemFamily $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('LINUX', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('LINUX', $this->_object->getName());
	}
	
	public function testGetBrand() : void
	{
		$this->assertEquals(new Brand('BRAND'), $this->_object->getBrand());
	}
	
	public function testEquals() : void
	{
		$family = new OperatingSystemFamily('LINUX', new Brand('BRAND'));
		
		$this->assertTrue($this->_object->equals($family));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OperatingSystemFamily('LINUX', new Brand('BRAND'));
	}
	
}
