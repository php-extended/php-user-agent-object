<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\DeviceType;
use PHPUnit\Framework\TestCase;

/**
 * DeviceTypeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\DeviceType
 *
 * @internal
 *
 * @small
 */
class DeviceTypeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var DeviceType
	 */
	protected DeviceType $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('MOBILE', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('MOBILE', $this->_object->getName());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals(new DeviceType('MOBILE')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new DeviceType('MOBILE');
	}
	
}
