<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\Brand;
use PhpExtended\UserAgent\Device;
use PhpExtended\UserAgent\DeviceType;
use PhpExtended\UserAgent\OperatingSystem;
use PhpExtended\UserAgent\OperatingSystemFamily;
use PhpExtended\UserAgent\RenderingEngine;
use PhpExtended\UserAgent\RenderingEngineFamily;
use PhpExtended\UserAgent\UserAgent;
use PhpExtended\UserAgent\UserAgentType;
use PhpExtended\Version\Version;
use PHPUnit\Framework\TestCase;

/**
 * UserAgentTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\UserAgent
 *
 * @internal
 *
 * @small
 */
class UserAgentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UserAgent
	 */
	protected UserAgent $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('Mozilla/5.0', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$this->_object->setName('Firefox');
		$this->assertEquals('Firefox', $this->_object->getName());
	}
	
	public function testGetType() : void
	{
		$this->assertNull($this->_object->getType());
		$type = new UserAgentType('BROWSER');
		$this->_object->setType($type);
		$this->assertEquals($type, $this->_object->getType());
	}
	
	public function testGetVersion() : void
	{
		$this->assertNull($this->_object->getVersion());
		$version = new Version(5, 0, 0);
		$this->_object->setVersion($version);
		$this->assertEquals($version, $this->_object->getVersion());
	}
	
	public function testGetDevice() : void
	{
		$this->assertNull($this->_object->getDevice());
		$device = new Device('LINUX', new DeviceType('DESKTOP'), new Brand('BRAND'));
		$this->_object->setDevice($device);
		$this->assertEquals($device, $this->_object->getDevice());
	}
	
	public function testGetOperatingSystem() : void
	{
		$this->assertNull($this->_object->getOperatingSystem());
		$ops = new OperatingSystem('DEBIAN', new Version(1, 2, 3), new OperatingSystemFamily('LINUX', new Brand('BRAND')));
		$this->_object->setOperatingSystem($ops);
		$this->assertEquals($ops, $this->_object->getOperatingSystem());
	}
	
	public function testGetRenderingEngine() : void
	{
		$this->assertNull($this->_object->getRenderingEngine());
		$eng = new RenderingEngine('GECKO', new Version(2, 3, 4), new RenderingEngineFamily('KHTML'));
		$this->_object->setRenderingEngine($eng);
		$this->assertEquals($eng, $this->_object->getRenderingEngine());
	}
	
	public function testGetApparitionDate() : void
	{
		$this->assertNull($this->_object->getApparitionDate());
		$date = new DateTimeImmutable();
		$this->_object->setApparitionDate($date);
		$this->assertEquals($date, $this->_object->getApparitionDate());
	}
	
	public function testGetDisparitionDate() : void
	{
		$this->assertNull($this->_object->getDisparitionDate());
		$date = new DateTimeImmutable();
		$this->_object->setDisparitionDate($date);
		$this->assertEquals($date, $this->_object->getDisparitionDate());
	}
	
	public function testGetHeaderValue() : void
	{
		$this->assertEquals('Mozilla/5.0', $this->_object->getHeaderValue());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals(new UserAgent('Mozilla/5.0')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UserAgent('Mozilla/5.0');
	}
	
}
