<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\UserAgent\RenderingEngine;
use PhpExtended\UserAgent\RenderingEngineFamily;
use PhpExtended\Version\Version;
use PHPUnit\Framework\TestCase;

/**
 * RenderingEngineTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\UserAgent\RenderingEngine
 *
 * @internal
 *
 * @small
 */
class RenderingEngineTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RenderingEngine
	 */
	protected RenderingEngine $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('GECKO/50.0.1', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('GECKO', $this->_object->getName());
	}
	
	public function testGetVersion() : void
	{
		$this->assertEquals(new Version(50, 0, 1), $this->_object->getVersion());
	}
	
	public function testGetFamily() : void
	{
		$expected = new RenderingEngineFamily('KHTML');
		
		$this->assertEquals($expected, $this->_object->getFamily());
	}
	
	public function testEquals() : void
	{
		$renderingEngine = new RenderingEngine('GECKO', new Version(50, 0, 1), new RenderingEngineFamily('KHTML'));
		
		$this->assertTrue($this->_object->equals($renderingEngine));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new RenderingEngine('GECKO', new Version(50, 0, 1), new RenderingEngineFamily('KHTML'));
	}
	
}
